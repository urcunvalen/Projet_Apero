<html lang="fr">
<head>
	<title>Projet Apéro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=PT+Sans+Narrow" />
	<link href="css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="css/normalize.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
	<script src="js/script.js"></script> 
</head>
<body>
<div id="entete">
<a href="index.php">
	<img src="img/logo.png"  id="logo">
</a>	
	<table id="links">
		<tr>
			<th class="link"><a href="messbox.php">Messagerie</a>   </th>
			<th class="link"><a href="creation.php">Créer une annonce</a>   </th>
			<th class="link"><a href="mesannonces.php">Mes annonces</a>   </th>
			<th class="link"><a href="index.php">Accueil</a>   </th>
		</tr>
	</table>
	<div id="recherche">
		<form id="formulaireRecherche" method="POST" action="index.php">
		<label>
			<input type="text" name="rech" id="rech" class="form-control" placeholder="Votre recherche ici">
			<input type="text" name="rechDate" id="rechDate" class="form-control" placeholder="Votre date ici">
			<script type="text/javascript">
				$('#rechDate').datepicker();
			</script>
		</label>
		<img src="img/calendar.png" onclick="changeRech()" id="datepicker" class="calendrier">
		<img src="img/loupe.png "onclick="document.forms.formulaireRecherche.submit()" id="Rechercher">
		</form>
	</div>
	<div id="compte">
<?php 
	session_start();
	if( isset($_SESSION["user"])&&isset($_SESSION['mdp'])){
	?><img class="photosProfil" src=<?php echo $_SESSION['photo'];?> >
      <a href=<?php echo'profil.php?pseudo='.$_SESSION['user']?>  class="linkProfil">Mon profil</a>
      <input type="button" name="deconnexion" id="deconnexion" onclick="deconnexion()" value="Déconnexion">  	
	<?php } else{
	?><div id="connexionForm">
	<form method="post" name="connexion">
	<input type="text" name="login" id="login" placeholder="Login">
	<br>
	<input type="password" name="mdp" id="mdp" placeholder="Mot de passe">
	<input type="button" value="Connexion" id="connexion" onclick="verifUser(document.getElementById('login').value,document.getElementById('mdp').value)">
	</form>
	</div>
 <button id="signup">Inscription</button>
 
 <?php }?>
</div>
</div>
<div id="profil">
<?php //Permet d'afficher l'intégralité du profil, si il s'agit du propriétaire du profil, sinon les informations publiques
	require("core.php");
	$userprof = $_GET['pseudo'];
	$db = connecterBDD("127.0.0.1","jeanmax","nR8cG08");
    mysqli_select_db($db,"2016_p0_cpi02_jeanmax");
    $user = $_SESSION['user'];
    $req = 	"SELECT * FROM Utilisateur u WHERE u.pseudo = '". $userprof."'"; 
    $res = mysqli_query($db,$req);
       while ($row = mysqli_fetch_assoc($res)) {
            $idUtil = $row['idUtil'];
            $tel = $row['tel'];
            $email = $row['email'];
            $photo = $row['photo'];
            $note = $row['note'];
            $adresse = $row['adresse'];
            $ville = $row['ville'];
            $fumeur = $row['fumeur'];
            $vapoteur = $row['vapoteur'];
            $alcool = $row['alcool'];
            $date = $row['dateNaissance'];
            $nom = $row['nom'];
            $prenom = $row['prenom'];
            $pseudo = $row['pseudo'];
            $description = $row['description'];
            $experience = $row['experience'];
        }   
    $fumeur = tick($fumeur);
    $vapoteur = tick($vapoteur);
    $alcool = tick($alcool);
	?>
	       <h1> Profil de <?php echo $pseudo;?></h1>
			<div id="photo">
			<img src=<?php echo $photo ?>>
		  </div>

	<div id="infosProfil">
	<?php
	
	if ($_SESSION['user']==$pseudo) { //Vérification si le visiteur du profil est le propriétaire ou non
	echo "<h3>Informations personnelles</h3><br>Nom :".$nom."</br>
	Prenom :".$prenom."</br>
	Telephone :".$tel."</br>
	Mail :".$email."</br><input type='button' name='modifProfil' value='Modifier le profil' id='buttonModif' onclick=modifierProfil('$pseudo')>";
	}

	echo "<h3>Informations publiques</h3><br>Ville : ".$ville."</br>
	Consomme de lalcool : ".$alcool."</br>
	Fumeur : ".$fumeur." </br>
	Vapoteur : ".$vapoteur."</br>
	Note : ".$note."/5</br>
	Date de naissance: ".$date."</br>
	Description : ".$description."</br>
	Experience : ".$experience."</br>
	</div>
	</div>";
	
	deconnecterBDD($db);
?>
</div>
</div>

<div id="inscriptionForm">

<?php 
//Permet d'afficher le bloc d'inscription
echo file_get_contents('inscription.php');

 ?>
	
</div>
</body>
</html>