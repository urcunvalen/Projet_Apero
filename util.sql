
CREATE TABLE Utilisateur (
  id int(11) NOT NULL AUTO_INCREMENT,
  tel varchar(12) DEFAULT NULL,
  email varchar(30) DEFAULT '',
  motDePasse varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT ROW_FORMAT=COMPACT;


CREATE TABLE Annonce (
  idAnnonce varchar(12)  NOT NULL,
  nInvite INT CHECK (nInvite BETWEEN 0 AND 10000),
  adresse varchar(100) NOT NULL,
  dateApero date,
  ville varchar(100) NOT NULL,
   
) 

ALTER TABLE listeInvites
    ADD FOREIGN KEY (idAnnonce) REFERENCES Annonce(idAnnonce) ;

/*Supprimer une foreign key*/
SHOW CREATE TABLE `table_name`:
ALTER TABLE table_name DROP FOREIGN KEY `table_name_ibfk_1`;
ALTER TABLE table_name DROP KEY `column_tablein_26440ee6`;


/* Ajouter une foreign key */
ALTER TABLE Annonce
    ADD idUtil INT,
    ADD FOREIGN KEY (idUtil) REFERENCES Utilisateur(idUtil) ;

CREATE TABLE Messagerie (
  id int(11) NOT NULL AUTO_INCREMENT,
  msg  varchar(500) NOT NULL,
  sendby varchar(20) NOT NULL,
  sendto varchar(20) NOT NULL,
  envoi date,
  PRIMARY KEY (id)
) 

CREATE TABLE MessGestion (
  id int(11) NOT NULL AUTO_INCREMENT,
  msg  varchar(500) NOT NULL,
  sendby varchar(20) NOT NULL,
  sendto varchar(20) NOT NULL,
  envoi date,
  PRIMARY KEY (id)
) 