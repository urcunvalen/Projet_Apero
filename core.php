<?php

    function connecterBDD($server, $user, $pass) {//connexion a la bdd

        $DBconn = mysqli_connect($server, $user, $pass);
        if (!$DBconn) {
            die("Erreur: " . mysqli_connect_error());
        }

        return $DBconn;
    }


    function deconnecterBDD($DBconn) {//deconnexion de la bdd
        if (isset($DBconn)) {
            mysqli_close($DBconn);
        }
    }
    /**
     *
     * @param $tab array Liste d'éléments, ex:
     * @return string
     */

    function tick($a){//affichage imagé des options de profil
        if ($a){
            $res = "<img class='tick' src = 'css/check.png'> </br>";
        }
        else{
            $res = "<img class='tick' src = 'css/cross.png'> </br>";
        }
        return $res;
    }

    function envoiMsg($data){
        $msg = $data['contenu'];
        return $msg;
    }

    function safe($var){//vérifie les champs pour empecher une requete potentielle envoyée par l'utilisateur
    $var = mysqli_real_escape_string($var);
    $var = addcslashes($var, '%_');
    $var = trim($var);
    $var = htmlspecialchars($var);
    return $var;
}
?>