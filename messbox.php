<html lang="fr">
<head>
	<title>Projet Apéro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=PT+Sans+Narrow" />
	<link href="css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="css/normalize.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
	<script src="js/script.js"></script> 
</head>
<body>	
<div id="entete">
<a href="index.php">
	<img src="img/logo.png"  id="logo">
</a>	
	<table id="links">
		<tr>
			<th class="link"><a href="messbox.php">Messagerie</a>   </th>
			<th class="link"><a href="creation.php">Créer une annonce</a>   </th>
			<th class="link"><a href="mesannonces.php">Mes annonces</a>   </th>
			<th class="link"><a href="index.php">Accueil</a>   </th>
			</tr>
	</table>
	<div id="recherche">
		<form id="formulaireRecherche" method="POST" action="index.php">
		<label>
			<input type="text" name="rech" id="rech" class="form-control" placeholder="Votre recherche ici">
			<input type="text" name="rechDate" id="rechDate" class="form-control" placeholder="Votre date ici">
			<script type="text/javascript">
				$('#rechDate').datepicker();
			</script>
		</label>
		<img src="img/calendar.png" onclick="changeRech()" id="datepicker" class="calendrier">
		<img src="img/loupe.png "onclick="document.forms.formulaireRecherche.submit()" id="Rechercher">
		</form>
	</div>		
<div id="compte">
<?php 
	session_start();
	if( isset($_SESSION["user"])&&isset($_SESSION['mdp'])){
	?><img class="photosProfil" src='<?php echo $_SESSION['photo'];?>' >
      <a href=<?php echo'profil.php?pseudo='.$_SESSION['user']?> class="linkProfil">Mon profil</a>
      <input type="button" name="deconnexion" id="deconnexion" onclick="deconnexion()" value="Déconnexion">  	
	<?php } else{
	?><div id="connexionForm">
	<form method="post" name="connexion">
	<input type="text" name="login" id="login" placeholder="Login">
	<br>
	<input type="password" name="mdp" id="mdp" placeholder="Mot de passe">
	<input type="button" value="Connexion" id="connexion" onclick="verifUser(document.getElementById('login').value,document.getElementById('mdp').value)">
	</form>
	</div>
 <button id="signup">Inscription</button>

 <?php }?>
</div>
</div>

<div id="inscriptionForm">

<?php 
echo file_get_contents('inscription.php');
 ?>


</div>
<br>
<br>
<br>
<br>
<br>
<br>

<?php 
	echo '<div id="destinataire">'; //ouvre le bloc destinaire
	require("core.php"); 
	if (isset($_SESSION['user'])) { //recupere les infos de l'utilisateur
	if ($_SESSION['admin'] == "1"){//si on est admin, alors on peut accéder a l'utilisateur que l'on veut
		echo 'Envoyeur : <input type="text" style="color:black" id="adminenvoi" value="">';
	}
	$envoyeur = $_SESSION['user'];
	$db = connecterBDD("127.0.0.1","jeanmax","nR8cG08");
    $b = mysqli_select_db($db,"2016_p0_cpi02_jeanmax");
	$contenu = "";	
	$date = date('Y/m/d');
	$modifdate = explode('/',$date);
    $datefin = $modifdate[0] . '-' . $modifdate[1] . '-' . $modifdate[2];
    echo 'Destinataire : ';
    if(empty($receveur)){//si le champ "receveur du message" est vide alors on met "inconnu"
    echo "<input id='recev' type='text' size ='15' style='color:black' value ='Inconnu'>";
	}
	else {			
	echo "<input id='recev' type='text' size ='15' style='color:black' value ='".$receveur."'></div>'";//sinon on remplit ce champ par son pseudo
	}
	?></div> 
	<div id="pageMessage">
	</div> 
	<script>
	envoyeur ='<?php echo $envoyeur ;?>';//on recupere le pseudo de l'envoyeur
	var date ='<?php echo $date ;?>';
	setInterval(function(){
	if (<?php echo $_SESSION['admin'];?> == "1"){ //si on est admin, on recupere le pseudo dans le champ prévu pour l'admin
	envoyeur = document.getElementById("adminenvoi").value;
	} 
	var receveur = document.getElementById("recev").value;
	verifchat(envoyeur,receveur)},500);//toutes les 0.5 secondes, on raffraichit la conversation
	$(function() {
    $("form").submit(function() { return false; });//on empeche de recharger la page par le bouton "Entrer"
	});
	</script>
	<form method="post" id="message" action ="">
	<input type="text" size="100" id="contenu" name="contenu" value=""/>
	<input type="button" id="envoi" value="valider" onclick="envoimsg(envoyeur,document.getElementById('recev').value,document.getElementById('contenu').value,date)"/>
	</form> <!-- on envoie le message dans la bbd et sur le site en cliquant sur le bouton-->
	<script>
	$("#contenu").keyup(function(event){//permet d'envoyer le message par le bouton "Entrer"
    if(event.keyCode == 13){
        $("#envoi").click();
    }
	});

	</script>
	</div>
<div id="events">
	<?php //permet de noter les aperos passés 
	$requete="SELECT * FROM Annonce a WHERE a.idAnnonce IN (SELECT idAnnonce FROM listeInvites l WHERE l.idUtil = (SELECT idUtil FROM Utilisateur u WHERE u.pseudo = '".$envoyeur."') AND (a.dateAnnonce < '".$datefin."'))";
    $resultat = mysqli_query($db,$requete);
    while( $row = mysqli_fetch_assoc($resultat)){//affichage des apéros passés mais non notés
    	$requete2="SELECT pseudo FROM Utilisateur u WHERE u.idUtil = ".$row['idUtil'];
    	$orga = mysqli_query($db,$requete2);
    	while( $row2 = mysqli_fetch_assoc($orga)){
    	?> Vous devez noter 
    	<?php
    	  echo $row2['pseudo'].' pour son apéro : "'.$row['titre'].'"'?> <br>
 		 <input type="range" id="note" min="0" max="5">
  		<input type="button" value="Noter" onclick="envoinote('<?php echo $row2['pseudo']?>',document.getElementById('note').value)">
  		<?php
    }
    }
	} else {
		echo "<h4> Veuillez vous connecter ou vous inscrire pour accéder à ce module </h4>";
	}
	?>
</div>
</body>
</html>