function getXHR() {
  var xhr = null;
  if (window.XMLHttpRequest) // FF & autres
     xhr = new XMLHttpRequest();
  else if (window.ActiveXObject) { // IE < 7
       try {
         xhr = new ActiveXObject("Msxml2.XMLHTTP");
       } catch (e) {
         xhr = new ActiveXObject("Microsoft.XMLHTTP");
       }
  } else { // Objet non supporté par le navigateur
     alert("Votre navigateur ne supporte pas AJAX");
     xhr = false;
  }
  return xhr;
}

function changeRech(){      //Permet de définir le type de recherche, par date ou par mots-clés
  if ($('#rechDate').css('visibility')=='hidden'){      //Vérification du type de recherche en cours
    $('#rech').css('visibility','');                    //On supprime la propriété visibility de l'élément #rech
    $('#rech').css('visibility','hidden');              //On cache #rech
    $('#rechDate').css('visibility','');                //On supprime la propriété visibility de l'élément #rechDate
    $('#rechDate').css('visibility','visible');         //On affiche rechDate
  } else {
    $('#rechDate').css('visibility','');                //On effectue la même manipulation dans le sens inverse
    $('#rechDate').css('visibility','hidden');
    $('#rech').css('visibility','');
    $('#rech').css('visibility','visible');
  }
}

function recupRech(){   //Permet de récupérer la recherche que l'on souhaite ( Par date ou par mots-clés)
  if ($('#rech').css('visibility')=='hidden'){
    var res =verifDate($('#rechDate').val(),'00-00-00',2);
  }else{
    var res =$('#rech').val();
  }
return res
}

function verifchat(envoyeur,receveur){
    // détection de la saisie dans le champ de recherche
      // on envoie la valeur recherché en POST au fichier de traitement
      $.ajax({
    type : 'POST', // envoi des données en POST
    url : 'updatechat.php' , // url du fichier de traitement
  data : 'envoyeur='+envoyeur+'&receveur='+receveur, // données à envoyer en post 
  beforeSend : function() { // traitements JS à faire AVANT l'envoi
  },
  success : function(data){ // traitements JS à faire APRES le retour d'ajax-search.php
    $('#pageMessage').html(data); // affichage des résultats dans le bloc
  }
      }); 
}

function envoimsg(envoyeur,receveur,contenu,date){
    // détection de la saisie dans le champ de recherche
      // on envoie la valeur recherché en POST au fichier de traitement
      $.ajax({
    type : 'POST', // envoi des données en POST
    url : 'sendchat.php', // url du fichier de traitement
  data : 'envoyeur='+envoyeur+'&receveur='+receveur+'&contenu='+contenu+'&date='+date, // données à envoyer en post 
  beforeSend : function() { // traitements JS à faire AVANT l'envoi
  },
  success : function(data){
    if (data == "\n"){
      $('#recev').val('Utilisateur incorrect');
      $('#recev').css('color', 'red');
    }else{
      $('#recev').css('color', 'black');
    }
    preced = $('#pageMessage').html();
    data = preced + data + '\n'; // traitements JS à faire APRES le retour d'ajax-search.php
    $('#pageMessage').html(data); // affichage des résultats dans le bloc
    $('#contenu').val('');
  }
      }); 
}

function verifDate(date,heure,type){//vérification de la date
  switch (type){//si on veut une date de type datetime (avec les secondes)
    case 1 :var res1 = date.split('/');//on sépare par /
            var res2= heure.split('h');//on sépare les heures
            res3=res1[2]+'-'+res1[0]+'-'+res1[1]+' '+res2[0]+':'+res2[1]+':00';//on redéfinit le format de la date 
    break;//sinon
    case 2 :var res1 = date.split('/');
            var res2= heure.split('h');
            res3=res1[2]+'-'+res1[0]+'-'+res1[1];//sans secondes
    break;
    }
  return res3;//on retourne la nouvelle heure
}

function executionRecherche(recherche){
  // détection de la saisie dans le champ de recherche
    $('#AnnoncesRecentes').html(''); // on vide les resultats
      // on envoie la valeur recherché en POST au fichier de traitement
      $.ajax({
    type : 'POST', // envoi des données en POST
    url : 'Recherche.php' , // url du fichier de traitement

  data : 'rech1='+recherche, // données à envoyer en post 
  beforeSend : function() { // traitements JS à faire AVANT l'envoi
  },
  success : function(data){ // traitements JS à faire APRES le retour d'ajax-search.php
    $('#annoncesRecentes').html(data); // affichage des résultats dans le bloc
  }
      });   
};


function executionInscription(nom, prenom, pseudo, motDePasse, motDePasse2, sexe, ville, photo, fumeur, vape, alcool, description, telephone,naissance, adresse, mail){
var xhr = getXHR();
  // On définit que l'on va faire à chq changement d'état
    xhr.onreadystatechange = function() {
     // On ne fait quelque chose que si on a tout reç̧u 
     // et que le serveur est ok
     if (xhr.readyState == 4 && xhr.status == 200){
        // traitement ré́alisé avec la réponse...
        var reponse = xhr.responseText;
        document.getElementById('compte').innerHTML = reponse;
     }
    } 
  // cas de la mé́thode post
  xhr.open("POST","addmembertodb.php",true) ;
  xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
  xhr.send("nom="+nom+"&prenom="+prenom+"&pseudo="+pseudo+"&motDePasse="+motDePasse+"&motDePasse2="+motDePasse2+"&sexe="+sexe+"&ville="+ville+"&photo="+photo+"&fumeur="+fumeur+"&vape="+vape+"&alcool="+alcool+"&description="+description+"&telephone="+telephone+"&naissance="+naissance+"&adresse="+adresse+"&mail="+mail);
}




function creationAnnonce(titre,theme,adresse, ville, date,heure,nbPersonnes,prix){
    var dateHeure = verifDate(date,heure,1);
    $('#creationAnnonce').html(''); // on vide les resultats
      // on envoie la valeur recherché en POST au fichier de traitement
      $.ajax({
    type : 'POST', // envoi des données en POST
    url : 'creationAnnonce.php', // url du fichier de traitement
    data : 'titre='+titre+"&theme="+theme+"&ville="+ville+"&adresse="+adresse+"&date="+date+"&heure="+heure+"&nbPersonnes="+nbPersonnes+"&prix="+prix , // données à envoyer en post 
  success : function(data){ // traitements JS à faire APRES le retour d'ajax-search.php
    $('#creationAnnonce').html(data); // affichage des résultats dans le bloc
  }
   });
} 

function verifUser(login,mdp){
    $('#compte').html(''); // on vide les resultats
      // on envoie la valeur recherché en POST au fichier de traitement
      $.ajax({
    type : 'POST', // envoi des données en POST
    url : 'connexion.php' , // url du fichier de traitement

  data : 'login='+login+'&mdp='+mdp , // données à envoyer en post 
  success : function(data){ // traitements JS à faire APRES le retour d'ajax-search.php
    $('#compte').html(data); // affichage des résultats dans le bloc
  }
      });  
}

function dispinfo(thisbutton,id){

  if (thisbutton.parentNode.lastChild.previousSibling.innerHTML == ""){
         $.ajax({
    type : 'POST', // envoi des données en POST
    url : 'affichplus.php' , // url du fichier de traitement
    data : 'idAnnonce='+id, // données à envoyer en post 
    beforeSend : function() { // traitements JS à faire AVANT l'envoi
  },
  success : function(data){ // traitements JS à faire APRES le retour d'ajax-search.php
    thisbutton.parentNode.lastChild.previousSibling.innerHTML = data; // affichage des résultats dans le bloc
  }
  }); 
  }else {   
    thisbutton.parentNode.lastChild.previousSibling.innerHTML ='';
  }
}

function inscritannonce(thisbutton,user,id){
    thisbutton.nextSibling.nextSibling.innerHTML = ""; // on vide les resultats
        // on envoie la valeur recherché en POST au fichier de traitement
        $.ajax({
        type : 'POST', // envoi des données en POST
        url : 'verifplace.php' , // url du fichier de traitement
        data : "user="+user+"&id="+id,
        success : function(data){ // traitements JS à faire APRES le retour d'ajax-search.php
          thisbutton.nextSibling.nextSibling.innerHTML = data; // affichage des résultats dans le bloc
  }
      });  
}

function envoinote(user,note){
        // on envoie la valeur recherché en POST au fichier de traitement
        
        $.ajax({
        type : 'POST', // envoi des données en POST
        url : 'calcnote.php' , // url du fichier de traitement
        data : "user="+user+"&note="+note,

        success : function(data){ // traitements JS à faire APRES le retour d'ajax-search.php
          $('#events').html(data); // affichage des résultats dans le bloc
  }
      });  
}



function verifexperience(user){
        // on envoie la valeur recherché en POST au fichier de traitement
        
        $.ajax({
        type : 'POST', // envoi des données en POST
        url : 'calcexper.php' , // url du fichier de traitement
        data : "user="+user,

        success : function(data){ // traitements JS à faire APRES le retour d'ajax-search.php
  }
      });  
}

function deconnexion(){
    $('#compte').html(''); // on vide les resultats
        // on envoie la valeur recherché en POST au fichier de traitement
        $.ajax({
        type : 'POST', // envoi des données en POST
        url : 'deconnexion.php' , // url du fichier de traitement
        data : "login=",
        success : function(data){ // traitements JS à faire APRES le retour d'ajax-search.php
          $('#compte').html(data); // affichage des résultats dans le bloc
  }
      });  
}

function modifierProfil(user){
    $('#infosProfil').html(''); // on vide les resultats
        // on envoie la valeur recherché en POST au fichier de traitement
        $.ajax({
        type : 'POST', // envoi des données en POST
        url : 'modifProfil.php' , // url du fichier de traitement
        data : "user="+user,
        success : function(data){ // traitements JS à faire APRES le retour d'ajax-search.php
          $('#infosProfil').html(data); // affichage des résultats dans le bloc
         }
  });  
}


$(document).ready(function(){
      $("button").click(function(){//si on clique sur le bouton d'inscription
          $("#inscriptionForm").animate({//le bloc d'inscription coulisse
              height: 'toggle',
          });
      });
});

