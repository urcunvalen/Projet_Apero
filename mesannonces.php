<html lang="fr">
<head>
	<title>Projet Apéro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=PT+Sans+Narrow" />
	<link href="css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="css/normalize.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
	<script src="js/script.js"></script> 
</head>
<body>	
<div id="entete">
<a href="index.php">
	<img src="img/logo.png"  id="logo">
</a>	
	<table id="links">
		<tr>
			<th class="link"><a href="messbox.php">Messagerie</a>   </th>
			<th class="link"><a href="creation.php">Créer une annonce</a>   </th>
			<th class="link"><a href="mesannonces.php">Mes annonces</a>   </th>
			<th class="link"><a href="index.php">Accueil</a>   </th>
		</tr>
	</table>
	<div id="recherche">
		<form id="formulaireRecherche" method="POST" action="index.php">
		<label>
			<input type="text" name="rech" id="rech" class="form-control" placeholder="Votre recherche ici">
			<input type="text" name="rech" id="rechDate" class="form-control" placeholder="Votre date ici">
			<script type="text/javascript">
				$('#rechDate').datepicker();
			</script>
		</label>
		<img src="img/calendar.png" onclick="changeRech()" id="datepicker" class="calendrier">
		<img src="img/loupe.png "onclick="document.forms.formulaireRecherche.submit()" id="Rechercher">
		</form>
	</div>
<div id="compte">
<?php 
	session_start();
	if( isset($_SESSION["user"])&&isset($_SESSION['mdp'])){
	?><img class="photosProfil" src=<?php echo $_SESSION['photo'];?> >
      <a href=<?php echo 'profil.php?pseudo='.$_SESSION['user']?> class="linkProfil">Mon profil</a>
      <input type="button" name="deconnexion" id="deconnexion" onclick="deconnexion()" value="Déconnexion">  	
	<?php } else{
	?><div id="connexionForm">
	<form method="post" name="connexion">
	<input type="text" name="login" id="login" placeholder="Login">
	<br>
	<input type="password" name="mdp" id="mdp" placeholder="Mot de passe">
	<input type="button" value="Connexion" id="connexion" onclick="verifUser(document.getElementById('login').value,document.getElementById('mdp').value)">
	</form>
	</div>
 <button id="signup">Inscription</button>
 
 <?php }?>
</div>
</div>

</div>

<div id="inscriptionForm">

<?php 

echo file_get_contents('inscription.php');

 ?>
	
</div>

</form>
</div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>



<div id="pageAnnonce">
<?php if (isset($_SESSION['user'])) { // Permet d'afficher toutes les annonces auquel l'utilisateur participe ou bien a créé
	require("core.php");
	$util = $_SESSION['user'];
	$db = connecterBDD("127.0.0.1","jeanmax","nR8cG08");				//connexion à la BDD puis exécution requête permettant de récupérer les annonces voulues
    $b = mysqli_select_db($db,"2016_p0_cpi02_jeanmax");

	$requete="SELECT * FROM Annonce a WHERE a.idAnnonce IN (SELECT idAnnonce FROM listeInvites l WHERE l.idUtil = (SELECT idUtil FROM Utilisateur u WHERE u.pseudo = '".$util."'))";
    $resultat = mysqli_query($db,$requete);
	// affichage d'un message "pas de résultats"
	if(mysqli_num_rows($resultat) ==  0){
	?>
    	<h4 style="text-align:center; margin:10px 0;">Vous ne participez à aucun apéro.</h4>
	<?php
	} else{
    // parcours et affichage des résultats
    while( $res = mysqli_fetch_assoc($resultat)){
    ?>
    	<form method="post" name="connexion" action="modifAnnonce.php">
        <div class="annonce">
            <p style="background-color: #B40404; text-align: center; font: large bold; color: white; "><?php echo utf8_encode( $res['titre'] ); ?></p>
            <table style="width: 100%;">
            <tr>
                <th>Date</th>
                <td><?php echo $res['dateAnnonce'] ; ?></td>
            </tr>
            <tr>
                <th>Ville</th>
                <td><?php echo $res['ville']; ?></td>
            </tr>
            <tr>
                <th>Theme</th>
                <td><?php echo $res['theme']; ?></td>
            </tr>           
            <tr>
                <th>Nombre d'invités</th>
                <td><?php echo $res['nInvite']; ?></td>
            </tr>
            <tr>
                <th>Prix</th>
                <td><?php echo $res['prix']; ?></td>
            </tr>
            </table>
        <input type="submit" name="Modifier" value="Modifier"  class="boutapero">
        </div>
        </form>
    <?php
    $_SESSION['annonce'] = $res['idAnnonce'];
	}
	}
	} else {		//Si l'utilisateur n'est pas connecté ou inscrit : message d'erreur empêchant celui-ci d'accéder au contenu
		echo "<h4> Veuillez vous connecter ou vous inscrire pour accéder à ce module  </h4>";		
	}
	?>
</div>
</body>
</html>