
<html lang="fr">
<head>
	<title>Projet Apéro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=PT+Sans+Narrow" />
	<link href="css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="css/normalize.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
	<script src="js/script.js"></script> 
</head>
<body onload="executionRecherche('<?php if (isset($_POST['rech'])||isset($_POST['rechDate'])){ echo $_POST['rech'].$_POST['rechDate']; } else { echo '';} ?>')">	
<div id="entete">
<a href="index.php">
	<img src="img/logo.png"  id="logo">
</a>	
		<table id="links">
			<tr>
				<th class="link"><a href="messbox.php">Messagerie</a>   </th>
				<th class="link"><a href="creation.php">Créer une annonce</a>   </th>
				<th class="link"><a href="mesannonces.php">Mes annonces</a>   </th>
				<th class="link"><a href="index.php">Accueil</a>   </th>
			</tr>
		</table>
<div id="compte">
<?php 
	session_start();				//Permet de vérifier si l'utilisateur est connecté sur une session, si oui affiche les infos nécessaires, sinon affiche un formulaire de connexion ou d'inscription
	if( isset($_SESSION["user"])&&isset($_SESSION['mdp'])){
	?>
	<script> verifexperience(<?php $_SESSION["user"]?>)</script>
	<img class="photosProfil" src='<?php echo $_SESSION['photo'];?>' >
      <a href="profil.php?pseudo=<?php echo $_SESSION['user']; ?>" class="linkProfil">Mon profil</a>
      <input type="button" name="deconnexion" id="deconnexion" onclick="deconnexion()" value="Déconnexion">  	
	<?php } else{
	?><div id="connexionForm">
	<form method="post" name="connexion">
	<input type="text" name="login" id="login" placeholder="Login">
	<br>
	<input type="password" name="mdp" id="mdp" placeholder="Mot de passe">
	<input type="button" value="Connexion" id="connexion" onclick="verifUser(document.getElementById('login').value,document.getElementById('mdp').value)"> <!--Permet la connexion d'un utilisateur-->
	</form>
	</div>
 <button id="signup">Inscription</button> <!-- Permet l'inscription -->
 
 <?php }?>
 </div>

<div id="recherche">
	<form id="formulaireRecherche" method="POST">
	<label>
		<input type="text" name="rech" id="rech" class="form-control" placeholder="Votre recherche ici">
		<input type="text" name="rech" id="rechDate" class="form-control" placeholder="Votre date ici">
		<script type="text/javascript">
			$('#rechDate').datepicker(); //Permet d'afficher un calendrier afin de sélectionner une date pour la recherche
		</script>
	</label>
	<img src="img/calendar.png" onclick="changeRech()" id="datepicker" class="calendrier">
	<img src="img/loupe.png "onclick="executionRecherche(recupRech())" id="Rechercher">  <!--Permet l'exécution de la recherche-->
	</form>
</div>
</div>

<div id="inscriptionForm">
<?php
	echo file_get_contents('inscription.php');
?>
	
</div>


<div id="annoncesRecentes"> 
	
</div> 
</body>
</html>