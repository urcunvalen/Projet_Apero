<html lang="fr">
<head>
	<title>Projet Apéro</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=PT+Sans+Narrow" />
	<link href="css/datepicker.css" rel="stylesheet" type="text/css">
	<link href="css/normalize.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
	<script src="js/script.js"></script> 
 	<script type="text/javascript">
		$('#datepicker').datepicker()
	</script>
</head>
<body>
<div id="entete">
<a href="index.php">
	<img src="img/logo.png"  id="logo">
</a>	
<table id="links">
		<tr>
			<th class="link"><a href="messbox.php">Messagerie</a>   </th>
			<th class="link"><a href="creation.php">Créer une annonce</a>   </th>
			<th class="link"><a href="mesannonces.php">Mes annonces</a>   </th>
			<th class="link"><a href="index.php">Accueil</a>   </th>
		</tr>
	</table>
	<div id="recherche">
		<form id="formulaireRecherche" method="POST" action="index.php">
		<label>
			<input type="text" name="rech" id="rech" class="form-control" placeholder="Votre recherche ici">
			<input type="text" name="rechDate" id="rechDate" class="form-control" placeholder="Votre date ici">
			<script type="text/javascript">
				$('#rechDate').datepicker(); //Permet l'apparition d'un calendrier pour sélectionner la date
			</script>
		</label>
		<img src="img/calendar.png" onclick="changeRech()" id="datepicker" class="calendrier">
		<img src="img/loupe.png "onclick="document.forms.formulaireRecherche.submit()" id="Rechercher"> <!-- Permet la recherche -->
		</form>
	</div>
<div id="compte">
<?php 
	session_start();//Permet de vérifier si l'utilisateur est connecté sur une session, si oui affiche les infos nécessaires, sinon affiche un formulaire de connexion ou d'inscription
	if( isset($_SESSION["user"])&&isset($_SESSION['mdp'])){
	?><img class="photosProfil" src=<?php echo $_SESSION['photo'];?> >
      <a href=<?php echo 'profil.php?pseudo='.$_SESSION['user']?> class="linkProfil">Mon profil</a>
      <input type="button" name="deconnexion" id="deconnexion" onclick="deconnexion()" value="Déconnexion">  	
	<?php } else{
	?><div id="connexionForm">
	<form method="post" name="connexion">
	<input type="text" name="login" id="login" placeholder="Login">
	<br>
	<input type="password" name="mdp" id="mdp" placeholder="Mot de passe">
	<input type="button" value="Connexion" id="connexion" onclick="verifUser(document.getElementById('login').value,document.getElementById('mdp').value)">
	</form>
	</div>
 <button id="signup">Inscription</button>
 
 <?php }?>
</div>
</div>
<div id="inscriptionForm">

<?php 
echo file_get_contents('inscription.php');
 ?>
</div>


<div id="creationAnnonce"> 
<?php if (isset($_SESSION['user'])) {
	?>
	<form method="POST" name="formCreation">
	<span>Donnez un titre à votre évènement afin que les utilisateurs puissent trouver celui-ci</span>
	<br>
		<input type="text" name="titre" id="titre" placeholder="Titre de l'évènement"><span class="infos"> * </span>
		<br>
		<span>Les soirées à thèmes sont très recherchées, à vous de trouver un thème original</span>
		<br>
		<input type="text" name="theme" id="theme" placeholder="Thème de l'évènement"><span class="infos"> * </span>
		<br>
		<span>Veuillez entrer une adresse valide, vérifier l'exactitude de celle-ci avant de confirmer la création</span>
		<br>
		<input type="text" name="villeo" id="villeo" placeholder="Ville"><span class="infos"> * </span>
		<br>
		<input type="text" name="adresses" id="adresses" placeholder="Adresse de l'évènement"><span class="infos"> * </span>
		<br>
		<span>Entrez la date et l'heure de l'évènement</span>
		<br>
		<input type="text" name="dates" placeholder="Date" id="dates"><span class="infos"> * </span>
		<br>
		<script>
			$( "#dates" ).datepicker();
		</script>

		<select name="heure" id="heure" placeholder="Heure de l'évènement">
        	<option value="0h">00h</option>
        	<option value="1h">01h</option>
        	<option value="2h">02h</option>
        	<option value="3h">03h</option>
        	<option value="4h">04h</option>
        	<option value="5h">05h</option>
        	<option value="6h">06h</option>
       	 	<option value="7h">07h</option>
       		<option value="8h">08h</option>
       		<option value="9h">09h</option>
        	<option value="10h">10h</option>
        	<option value="11h">11h</option>
        	<option value="12h">12h</option>
        	<option value="13h">13h</option>
        	<option value="14h">14h</option>
        	<option value="15h">15h</option>
        	<option value="16h">16h</option>
        	<option value="17h">17h</option>
       		<option value="18h">18h</option>
        	<option value="19h">19h</option>
        	<option value="20h">20h</option>
        	<option value="21h">21h</option>
        	<option value="22h">22h</option>
        	<option value="23h">23h</option>
        </select>
        <select name="minute" id="minute" placeholder="Heure de l'évènement">
        	<option value="0">00</option>
        	<option value="15">15</option>
        	<option value="30">30</option>
        	<option value="45">45</option>
        </select><span> * </span>
		<br>
		<span>Combien de personnes souhaitez-vous accueillir?</span>
		<br>
		<input type="number" name="nbPersonnes" id="nbPersonnes" placeholder="Nombre de places maximum" min="0"><span class="infos"> * </span>
		<br>
		<span>L'un des critères les plus importants, quel sera le prix de votre soirée</span>
		<br>
		<input type="number" name="prix" id="prix" placeholder="Prix de l'évènement" min="0"><span class="infos"> * </span>
		<br>
		<input type="button" name="envoyer" value="Créer l'annonce" onclick="creationAnnonce(document.getElementById('titre').value,document.getElementById('theme').value,document.getElementById('adresses').value, document.getElementById('villeo').value, document.getElementById('dates').value,document.getElementById('heure').value+document.getElementById('minute').value,document.getElementById('nbPersonnes').value,document.getElementById('prix').value)">
	</form>
	<?php } else {
    echo "<h4> Veuillez vous connecter ou vous inscrire pour accéder à ce module  </h4>";	
	}
    ?>
</div> 
<div id="inscriptionForm">

<?php 
//Permet d'afficher le bloc d'inscription
echo file_get_contents('inscription.php');

 ?>
	
</div>
</body>
</html>